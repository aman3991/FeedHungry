function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: {lat: 12.9007, lng: 80.1969}
    });

    // Create an array of alphabetical characters used to label the markers.
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    // Add some markers to the map.
    // Note: The code uses the JavaScript Array.prototype.map() method to
    // create an array of markers based on a given "locations" array.
    // The map() method here has nothing to do with the Google Maps API.
    var markers = locations.map(function(location, i) {
      return new google.maps.Marker({
        position: location,
        label: labels[i % labels.length]
      });
    });

    // Add a marker clusterer to manage the markers.
    var markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
  }
  var locations = [
    {lat: 12.9007, lng: 80.1916},
    {lat: 12.9008, lng: 80.1978},
    {lat: 12.9005, lng: 80.1996},
    {lat: 12.9008, lng: 80.1978},
    {lat: 12.9006, lng: 80.1945},
    {lat: 12.9003, lng: 80.1963},
    {lat: 12.9001, lng: 80.1948},
    {lat: 12.9002, lng: 80.1945},
    {lat: 12.9004, lng: 80.1963},
    {lat: 12.9008, lng: 80.1941},
    {lat: 12.9009, lng: 80.1974},
    {lat: 12.9004, lng: 80.1973},
    {lat: 12.9008, lng: 80.1914},
    {lat: 12.9006, lng: 80.1901},
    {lat: 12.9003, lng: 80.1930},
    {lat: 12.9002, lng: 80.1978},
    {lat: 12.9001, lng: 80.1998},
    {lat: 12.9004, lng: 80.1978},
    {lat: 12.9007, lng: 80.1961},
    {lat: 12.9008, lng: 80.1961},
    {lat: 12.9003, lng: 80.1945},
    {lat: 12.9008, lng: 80.1963},
    {lat: 12.9007, lng: 80.1902}
  ]